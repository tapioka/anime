# _*_ coding: utf-8 _*_ 
require 'anemone' 
require 'nokogiri' 
require 'open-uri'
require 'kconv' 
require 'timeout'
require 'uri'
# railsではロードパスのルートがlibになる
require_dependency 'scraper.rb'
class Tasks::Fc2Scraper < Tasks::MatomeBase
  @base_url = 'http://tvanimedouga.blog93.fc2.com/'
  @animes = get_latest_anime()

  def self.get_new_anime()
    doc=get_doc(@base_url)
    animes = doc.xpath("//div/div[@id='baseBlock']/div[@id='baseLeft']/div[@id='menu1Block']/div[@class='menu1BlockBase']/div[@class='menu1BlockBody']/div[@class='menuText']/div[@class='plugin-freearea']/ul/li/img")
    animes.each do |anime|
      anime_link = anime.previous_sibling
      
      if anime_link == nil || anime_link == ""
        @@logger.debug 'アニメリンクAタグがありません'
        next
      end
      if anime_link[:href] == nil
        @@logger.debug 'アニメリンクhrefがありません'
        next
      end
      added_anime = AnimeMaster.where(title: anime_link.text)
      if added_anime.count > 0
        @@logger.debug '登録済みのアニメです ' + anime_link.text
        update_anime = added_anime.first
        update_anime.state = 1
        update_anime.save!


        next
      end
      detail = get_doc(anime_link[:href])
      arasuji_node = detail.xpath("//div/div[@id='baseBlock']/div[@id='baseLeft']/div[@id='mainBlock']/div[@class='mainEntryBlock']/div[@class='mainEntryBase']/div[@class='mainEntryBody']")
      arasuji = arasuji_node.text
      arasuji.slice!(arasuji_node.css("b").text)
      kiji_node = detail.xpath("//div/div[@id='baseBlock']/div[@id='baseLeft']/div[@id='mainBlock']/div[@class='mainEntryBlock']/div[@class='mainEntryBase']/div[@class='mainEntrykiji']")
      if kiji_node.text =~ /((\d+)\/(\d+))\(/ || kiji_node.text =~ /((\d+)月(\d+)日)/
        now = Date.today
        start_at = Date.new(now.year.to_i,$2.to_i,$3.to_i+1)
      else
        @@logger.info '放送開始日を取得できませんでした ' + anime_link.text
        start_at = Date.today

      end
        day_of_week = start_at.wday 
      # day_of_week = anime.parent.previous_sibling.css("b")

      # p day_of_week
      AnimeMaster.create!(title: anime_link.text, start_at: start_at, discription: arasuji, state: 1, day_of_week: day_of_week )
      @@logger.info 'アニメを登録しました。 \n ' + 'title ' + anime_link.text 
      

      # p arasuji_node.text
      # p arasuji_node.css("b").text

    end

  end
  # 目的のアニメのリンクを取得
  def self.get_anime_links(doc)
    animes = doc.xpath("//div/div[@id='baseBlock']/div[@id='baseLeft']/div[@id='menu1Block']/div[@class='menu1BlockBase']/div[@class='menu1BlockBody']/div[@class='menuText']/div[@class='plugin-freearea']/ul/li/a")
    return animes
  end

  # スクレイプ対象のアニメか確認
  def self.can_scrape_anime(anime_link)
    if anime_link == nil || anime_link == ""
      @@logger.debug 'アニメリンクAタグがありません'
      return nil
    end
    if anime_link[:href] == nil
      @@logger.debug 'アニメリンクhrefがありません'
      return nil
    end

    anime = @animes.where(title: anime_link.text)
    
    if anime.count > 0
      anime = anime.first
      @@logger.debug anime.title
      return anime
    end
    @@logger.debug '対象のアニメではありません'+anime_link.text
    return nil

  end



  # 動画のAタグを取得
  def self.get_movie_list_links(doc)
    movies = doc.xpath("//div/div[@id='baseBlock']/div[@id='baseLeft']/div[@id='mainBlock']/div[@class='mainEntryBlock']/div[@class='mainEntryBase']/div[@class='mainEntrykiji']/a")
    return movies
  end

  # スクレイプ対象の話か確認(OKならEpisodeモデルを返す)
  def self.get_episode(movie_list_link,anime)
    if movie_list_link == nil || movie_list_link == ""
      @@logger.debug '動画リストリンクAタグがありません'
      return nil
    end
    if movie_list_link[:href] == nil
      @@logger.debug '動画リストリンクhrefがありません'
      return nil
    end

    if movie_list_link.text =~ /^(\d+)話/
      chapter = $1
    end
    if chapter == nil
      @@logger.debug 'チャプターが見つかりませんでした'
      return nil
    end

    episode_title = get_episode_title(movie_list_link)
    
    episode = Episode.where(anime_master_id: anime.id, chapter: chapter)
    if episode.count > 0
      episode = episode.first
      @@logger.debug anime.title + chapter + '話'
      if episode_title != nil && episode_title != "" && (episode.title == nil || episode.title == "")
        episode.title = episode_title
        @@logger.debug 'エピソードタイトル更新' + episode_title
        episode.save!
      end

    else
      episode = Episode.new(anime_master_id: anime.id, chapter: chapter)
      if episode_title != nil && episode_title != ""
        episode.title = episode_title
      end
        episode.save!
        @@logger.debug anime.title+chapter+'話を新規作成します'

    end
    return episode
  end

  # 各話タイトルを取得
  def self.get_episode_title(movie)
    if movie[:title] =~ /「(.*)」/ 
      chapter_title = $1
      @@logger.debug 'タイトル取得　'+chapter_title
      return chapter_title
    end
    return nil
  end



  # リンクを取得
  def self.get_movie_links(movie_doc)
    movie = movie_doc.xpath("//div/div[@id='baseBlock']/div[@id='baseLeft']/div[@id='mainBlock']/div[@class='mainEntryBlock']/div[@class='mainEntryBase']/div[@class='mainEntrykiji']/a")
    return movie
  end

  # スクレイプ対象の動画か確認
  def self.get_movie_model(movie_link,episode)
    if movie_link == nil || movie_link == ""
      @@logger.debug '動画リンクAタグがありません'
      return nil
    end
    if movie_link[:href] == nil
      @@logger.debug '動画リンクhrefがありません'
      return nil
    end
    movie_name = nil
    if movie_link.text =~ /【(?!.*検索)(.*)】/
      @@logger.debug $1
      movie_name = $1
    end
    if movie_name == nil
      @@logger.debug '動画名が見つかりません'
      return nil
    end

    if !is_scrape_movie(movie_name,episode.anime_master_id,episode.chapter)
      @@logger.debug '登録済みの動画です'
      return nil
    end

    # if is_deleted_movie(movie_link[:href],movie_name)
    #   @@logger.debug '動画は削除されています'
    #   return nil
    # end

    movie = Movie.new(episode_id:episode.id,name:movie_name,url:movie_link[:href])
    return movie
  end

  def self.is_deleted_movie(url,movie_name)
    return Scraper.is_deleted_movie(url,movie_name)
  end

end

