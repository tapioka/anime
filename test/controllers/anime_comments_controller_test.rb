require 'test_helper'

class AnimeCommentsControllerTest < ActionController::TestCase
  setup do
    @anime_comment = anime_comments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:anime_comments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create anime_comment" do
    assert_difference('AnimeComment.count') do
      post :create, anime_comment: { anime_master_id: @anime_comment.anime_master_id, comment: @anime_comment.comment, is_valid: @anime_comment.is_valid, name: @anime_comment.name }
    end

    assert_redirected_to anime_comment_path(assigns(:anime_comment))
  end

  test "should show anime_comment" do
    get :show, id: @anime_comment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @anime_comment
    assert_response :success
  end

  test "should update anime_comment" do
    patch :update, id: @anime_comment, anime_comment: { anime_master_id: @anime_comment.anime_master_id, comment: @anime_comment.comment, is_valid: @anime_comment.is_valid, name: @anime_comment.name }
    assert_redirected_to anime_comment_path(assigns(:anime_comment))
  end

  test "should destroy anime_comment" do
    assert_difference('AnimeComment.count', -1) do
      delete :destroy, id: @anime_comment
    end

    assert_redirected_to anime_comments_path
  end
end
