class Admin::AnimeMastersController < ApplicationController
  before_action :set_anime_master, only: [:show, :edit, :update, :destroy]

  # GET /anime_masters
  # GET /anime_masters.json
  def index
    @anime_masters = AnimeMaster.page(params[:page])
  end

  # GET /anime_masters/1
  # GET /anime_masters/1.json
  def show
  end

  # GET /anime_masters/new
  def new
    @anime_master = AnimeMaster.new
  end

  # GET /anime_masters/1/edit
  def edit
  end

  # POST /anime_masters
  # POST /anime_masters.json
  def create
    @anime_master = AnimeMaster.new(anime_master_params)

    respond_to do |format|
      if @anime_master.save
        format.html { redirect_to admin_anime_master_url(id:@anime_master.id), notice: 'Anime master was successfully created.' }
        format.json { render :show, status: :created, location: @anime_master }
      else
        format.html { render :new }
        format.json { render json: @anime_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /anime_masters/1
  # PATCH/PUT /anime_masters/1.json
  def update
    respond_to do |format|
      if @anime_master.update(anime_master_params)
        format.html { redirect_to admin_anime_master_url(id:@anime_master.id), notice: 'Anime master was successfully updated.' }
        format.json { render :show, status: :ok, location: @anime_master }
      else
        format.html { render :edit }
        format.json { render json: @anime_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /anime_masters/1
  # DELETE /anime_masters/1.json
  def destroy
    @anime_master.destroy
    respond_to do |format|
      format.html { redirect_to admin_anime_masters_url, notice: 'Anime master was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_anime_master
      @anime_master = AnimeMaster.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def anime_master_params
      params.require(:anime_master).permit(:title, :start_at, :discription, :initial_id, :state, :day_of_week, :image_path)
    end
end
