class RenameAnimeIdColumnToEpisodes < ActiveRecord::Migration
  def change
    rename_column :episodes, :anime_id, :anime_master_id
  end
end
