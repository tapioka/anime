class CreateEpisodeComments < ActiveRecord::Migration
  def change
    create_table :episode_comments do |t|
      t.string :name
      t.text :comment
      t.boolean :is_valid
      t.belongs_to :episode, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
