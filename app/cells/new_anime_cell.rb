class NewAnimeCell < Cell::ViewModel
  def show
    # new_episodes = Episode.find_by_sql(
    #   "
    #     SELECT episodes.*,anime_masters.title as anime_title FROM episodes 
    #     INNER JOIN anime_masters 
    #     ON episodes.anime_master_id = anime_masters.id 
    #     AND anime_masters.latest_chapter = episodes.chapter
    #     AND state = 1
    #     ORDER BY episodes.updated_at DESC
    #   "
    # )
    new_episodes = Episode.joins(:anime_master)
                          .select("episodes.id,anime_masters.title as anime_title ,episodes.chapter,episodes.updated_at")
                          .where("anime_masters.latest_chapter <= episodes.chapter and anime_masters.state=1")
                          .order("episodes.updated_at DESC")
    render locals: { new_episodes: new_episodes}
  end

end
