# _*_ coding: utf-8 _*_ 
require 'anemone' 
require 'nokogiri' 
require 'open-uri'
require 'kconv' 
require 'timeout'
class Tasks::AnimeScrape
  def self.is_deleted_B9(url)
    doc= getDoc(url,"GBK")
    title = doc.xpath("//title").text
    p title
    if title =~ /削除/
      p "削除されました。"
      return true;
    end
    return false;
  end
  def self.is_deleted_himawari(url)
    doc = getDoc(url)
    title= doc.xpath("//div[@id='link_disablemessage_rights']").text
    if title =~ /削除/
      p "削除されました。"
      return true;
    end
    return false;
  end
  def self.is_deleted_youtube(url)
    doc = getDoc(url)
    title= doc.xpath("//h1").text
    if title =~ /再生できません/
      p "削除されました。"
      return true;
    end
    return false;
  end
  def self.is_deleted_anitube(url)
    doc = getDoc(url)
    title= doc.xpath("//div").text
    p title
    if title =~ /Video inexistente, pode ter sido apagado ou marcado como inapropriado!/
      p "削除されました。"
      return true;
    end
    return false;
  end


  def self.getDoc(url,charset=nil)
      sleep(1)
      userAgent = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)'
    begin
        html = open(url, 'User-Agent' =>userAgent) do |f|
          if charset == nil
            charset = f.charset # 文字種別を取得
          end
          f.read # htmlを読み込んで変数htmlに渡す
        end
      doc = Nokogiri::HTML.parse(html,nil,charset) 
    rescue => exception
      case exception
        when OpenURI::HTTPError # 404エラ
          puts "404エラー"+url
        else
          puts "その他エラー"
          puts exception.to_s
      end
      return nil
    end
    return doc
  end
  def self.execute
    titles =""

    anime_list_url="http://tvanimedouga.blog93.fc2.com/blog-entry-1028.html"
    doc = getDoc(anime_list_url)
    puts doc.xpath("//title")
    temp_links = doc.xpath("//*[@class='mainEntrykiji']/a")

    # アニメ一覧のループ
    temp_links.each do |link|
      @anime = AnimeMaster.find_by(title:link.text)
      if @anime!=nil && link[:href] != nil
        if @anime.id <= 223
          next
        end
        detail = getDoc(link[:href])
        if detail == nil
          next 
        end
        arasuji = detail.xpath("//*[@class='mainEntryBody']").inner_text
        @anime.discription=arasuji
        @anime.save!
        episodes = detail.xpath("//*[@class='mainEntrykiji']/a")
        episodes.each do |episode|
          if episode.text=~/(\d+)話/ && episode[:href] != nil

            movies_doc =  getDoc(episode[:href])
            if movies_doc == nil
              next
            end
            episode_id=""
            exists_episode=Episode.find_by(anime_master_id:@anime.id,chapter:$1)
            if exists_episode==nil
              anime_episode=Episode.new()
              anime_episode.chapter=$1
              anime_episode.title=episode.text
              anime_episode.anime_master_id=@anime.id
              anime_episode.save!
              episode_id=anime_episode.id
            else
              exists_episode.chapter=$1
              exists_episode.title=episode.text
              exists_episode.anime_master_id=@anime.id
              exists_episode.id=exists_episode.id
              exists_episode.save!
              episode_id=exists_episode.id
            end

            movies = movies_doc.xpath("//*[@class='mainEntrykiji']/a")
            movies.each do |movie|
              if movie.text=~/【(.*)】/ && movie[:href] != nil
                url = movie[:href]
                exists_movie=Movie.find_by(url:url)
                if exists_movie==nil
                  anime_movies=Movie.new()
                  anime_movies.episode_id=episode_id
                  anime_movies.url=url
                  anime_movies.name=$1
                  anime_movies.save!
                else
                  exists_movie.episode_id=episode_id
                  exists_movie.url=movie[:href]
                  exists_movie.id=exists_movie.id
                  exists_movie.name=$1
                  exists_movie.save!
                end

              end
            end
          end
        end
        puts "完了"
      end
    end
  end
  opts = {
    :skip_query_strings => true,
    :depth_limit => 0,
  }
  #    Anemone.crawl(anime_list_url, opts) do |anemone| 
  #      anemone.on_pages_like(%r[http://tvanimedouga.blog93.fc2.com/blog-entry-1028.html]) do |page|
  #        doc = nokogiri::html.parse(page.body.toutf8) 
  #        # 条件に一致するリンクだけ残す
  #        # この `links` はanemoneが次にクロールする候補リスト
  #        anemone.focus_crawl do |focus_page|
  #          links = doc.xpath("//*[@class='mainEntrykiji']/a")
  #          links.each do |link|
  #            @anime_masters.each do |anime|
  #              if anime.title==link.text && link[:href]!=nil
  #                if link[:href]=~/blog-entry-(\d+)/
  #                  titles=titles+$1+"|"
  #                end
  #              end
  #            end
  #          end
  #          titles.chop!
  #          keep_titles="/blog-entry-["+titles+"]/"
  #          focus_page.links.keep_if { |link|
  #            link.to_s.match(keep_titles)
  #          } 
  #          titles="%r[http://tvanimedouga.blog93.fc2.com/blog-entry-["+titles+"].html]"
  #          puts titles
  #        end 
  #      end
  #      if titles != ""
  #        anemone.on_pages_like(titles) do |page|
  #          doc = Nokogiri::HTML.parse(page.body.toutf8) 
  #          arasuji = doc.xpath("//*[@class='mainEntryBody']").text
  #          episode = doc.xpath("//*[@class='mainEntrykiji']/a").text
  #          puts "あらすじ："+arasuji
  #          puts ""
  #          puts episode
  #        end
  #      end
  #
  #      anemone.on_every_page do |page| 
  #        
  #      end 
  #    end 
  #  end 
  # 実行したいコードを書く
  #     p "Hello world"
  #       end
  #       end
end

