json.array!(@anime_comments) do |anime_comment|
  json.extract! anime_comment, :id, :name, :comment, :is_valid, :anime_master_id
  json.url anime_comment_url(anime_comment, format: :json)
end
