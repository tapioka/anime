json.array!(@movies) do |movie|
  json.extract! movie, :id, :name, :url, :episode_id
  json.url movie_url(movie, format: :json)
end
