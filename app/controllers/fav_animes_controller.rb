class FavAnimesController < ApplicationController

  def add
    id = params[:id]
    # ids=cookies[:fav_animes]
    # if !ids.instance_of?(Array)
    #   ids = Array.new
    # end
    # ids.push(id.to_i)
    if id != "" || id != nil 
    @fav_animes = self.after_action(id)
    render :partial =>  "fav_animes/show"
    else
      render :nothing => true
    end
  end
  def del
    ids=cookies[:fav_animes]
    if !ids.instance_of?(Array)
      ids = Array.new
    end
    ids.delete(params[:id])
    @fav_animes = self.after_action(ids)
    render :partial =>  "fav_animes/show"

  end
  def show
    id=cookies[:cView]
    @fav_animes = self.after_action(id)
    render :partial =>  "fav_animes/show"

  end
  def after_action(comma_id)
    # if ids.instance_of?(Array)
    #   comma_id = ""
    #   ids.each do |id|
    #     comma_id = id.to_s + ','
    #   end
    #   comma_id.chop!
    # else
    #   comma_id = (ids == '' || ids == nil) ? "" : ids
    # end
    if comma_id == "" 
      animes = AnimeMaster.none
    else
      animes = AnimeMaster.where("id IN("+comma_id+") AND state=1").order("day_of_week")
    end
    return animes
  end
end
