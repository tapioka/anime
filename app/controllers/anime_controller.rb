class AnimeController < ApplicationController
  def index
    @animes = AnimeMaster.where("state=1")
    # render layout: 'anime_top'
  end
  def show 
    @anime = AnimeMaster.find(params[:id])
    @episodes = Episode.where(anime_master_id: params[:id]).order(chapter: :desc)
    @new_comment = AnimeComment.new
    @new_comment.anime_master_id = params[:id]
    @anime_comments = AnimeComment.where(anime_master_id: params[:id]).order("updated_at desc")
  end
end
