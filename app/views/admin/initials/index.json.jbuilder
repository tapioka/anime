json.array!(@initials) do |initial|
  json.extract! initial, :id, :name
  json.url initial_url(initial, format: :json)
end
