class AnimeCommentsController < ApplicationController
  before_action :set_anime_comment, only: [:show, :edit, :update, :destroy]

  # GET /anime_comments
  # GET /anime_comments.json
  def index
    @anime_comments = AnimeComment.all
  end

  # GET /anime_comments/1
  # GET /anime_comments/1.json
  def show
  end

  # GET /anime_comments/new
  def new
    @anime_comment = AnimeComment.new
  end

  # GET /anime_comments/1/edit
  def edit
  end

  # POST /anime_comments
  # POST /anime_comments.json
  def create
    @anime_comment = AnimeComment.new(anime_comment_params)

    respond_to do |format|
      if @anime_comment.save
        format.html { redirect_to @anime_comment, notice: '下記の内容でコメントを投稿しました' }
        format.json { render :show, status: :created, location: @anime_comment }
      else
        format.html { render :new }
        format.json { render json: @anime_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /anime_comments/1
  # PATCH/PUT /anime_comments/1.json
  def update
    respond_to do |format|
      if @anime_comment.update(anime_comment_params)
        format.html { redirect_to @anime_comment, notice: 'Anime comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @anime_comment }
      else
        format.html { render :edit }
        format.json { render json: @anime_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /anime_comments/1
  # DELETE /anime_comments/1.json
  def destroy
    @anime_comment.destroy
    respond_to do |format|
      format.html { redirect_to anime_comments_url, notice: 'Anime comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_anime_comment
      @anime_comment = AnimeComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def anime_comment_params
      params.require(:anime_comment).permit(:name, :comment, :is_valid, :anime_master_id)
    end
end
