class ChangeDatatypeInitialOfAnimeMaster < ActiveRecord::Migration
  def change
    change_column :anime_masters, :initial, :integer
  end
end
