class Episode < ActiveRecord::Base
  belongs_to :anime_master
  has_many :movies,dependent: :destroy
end
