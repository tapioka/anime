class CreateAnimeComments < ActiveRecord::Migration
  def change
    create_table :anime_comments do |t|
      t.string :name
      t.text :comment
      t.boolean :is_valid, :null => false, :default => false
      t.belongs_to :anime_master, index: true, foreign_key: true, :dependent => :destroyend

      t.timestamps null: false
    end
  end
end
