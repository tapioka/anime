require 'test_helper'

class AnimeMastersControllerTest < ActionController::TestCase
  setup do
    @anime_master = anime_masters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:anime_masters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create anime_master" do
    assert_difference('AnimeMaster.count') do
      post :create, anime_master: { discription: @anime_master.discription, start_at: @anime_master.start_at, title: @anime_master.title }
    end

    assert_redirected_to anime_master_path(assigns(:anime_master))
  end

  test "should show anime_master" do
    get :show, id: @anime_master
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @anime_master
    assert_response :success
  end

  test "should update anime_master" do
    patch :update, id: @anime_master, anime_master: { discription: @anime_master.discription, start_at: @anime_master.start_at, title: @anime_master.title }
    assert_redirected_to anime_master_path(assigns(:anime_master))
  end

  test "should destroy anime_master" do
    assert_difference('AnimeMaster.count', -1) do
      delete :destroy, id: @anime_master
    end

    assert_redirected_to anime_masters_path
  end
end
