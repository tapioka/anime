class AddLatestChapterToAnimeMaster < ActiveRecord::Migration
  def change
    add_column :anime_masters, :latest_chapter, :integer
  end
end
