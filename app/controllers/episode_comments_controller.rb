class EpisodeCommentsController < ApplicationController
  before_action :set_episode_comment, only: [:show, :edit, :update, :destroy]
  # GET /episode_comments/1
  # GET /episode_comments/1.json
  def show
  end

  # GET /episode_comments/new
  def new
    @episode_comment = EpisodeComment.new
  end
  # POST /episode_comments
  # POST /episode_comments.json
  def create
    @episode_comment = EpisodeComment.new(episode_comment_params)

    respond_to do |format|
      if @episode_comment.save
        format.html { redirect_to episode_comment_path(@episode_comment.id), notice: 'Episode comment was successfully created.' }
        format.json { render :show, status: :created, location: @episode_comment }
      else
        format.html { render :new }
        format.json { render json: @episode_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /episode_comments/1
  # PATCH/PUT /episode_comments/1.json
  def update
    respond_to do |format|
      if @episode_comment.update(episode_comment_params)
        format.html { redirect_to @episode_comment, notice: 'Episode comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @episode_comment }
      else
        format.html { render :edit }
        format.json { render json: @episode_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /episode_comments/1
  # DELETE /episode_comments/1.json
  def destroy
    @episode_comment.destroy
    respond_to do |format|
      format.html { redirect_to episode_comments_url, notice: 'Episode comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_episode_comment
      @episode_comment = EpisodeComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def episode_comment_params
      params.require(:episode_comment).permit(:name, :comment, :is_valid, :episode_id)
    end
end
