class CreateAnimeMasters < ActiveRecord::Migration
  def change
    create_table :anime_masters do |t|
      t.string :title
      t.date :start_at
      t.text :discription

      t.timestamps null: false
    end
  end
end
