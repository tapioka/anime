json.array!(@temp_animes) do |temp_anime|
  json.extract! temp_anime, :id, :title, :start_at, :discription, :initial_id, :state, :day_of_week, :image_path
  json.url temp_anime_url(temp_anime, format: :json)
end
