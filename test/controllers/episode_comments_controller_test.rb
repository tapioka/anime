require 'test_helper'

class EpisodeCommentsControllerTest < ActionController::TestCase
  setup do
    @episode_comment = episode_comments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:episode_comments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create episode_comment" do
    assert_difference('EpisodeComment.count') do
      post :create, episode_comment: { comment: @episode_comment.comment, episode_id: @episode_comment.episode_id, is_valid: @episode_comment.is_valid, name: @episode_comment.name }
    end

    assert_redirected_to episode_comment_path(assigns(:episode_comment))
  end

  test "should show episode_comment" do
    get :show, id: @episode_comment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @episode_comment
    assert_response :success
  end

  test "should update episode_comment" do
    patch :update, id: @episode_comment, episode_comment: { comment: @episode_comment.comment, episode_id: @episode_comment.episode_id, is_valid: @episode_comment.is_valid, name: @episode_comment.name }
    assert_redirected_to episode_comment_path(assigns(:episode_comment))
  end

  test "should destroy episode_comment" do
    assert_difference('EpisodeComment.count', -1) do
      delete :destroy, id: @episode_comment
    end

    assert_redirected_to episode_comments_path
  end
end
