class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # before_action :get_this_season_animes
  # def get_this_season_animes
  #   @this_week_animes=AnimeMaster.where(state:1).order("day_of_week")
  #   @day_of_week = ["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"]
  # end
  before_action :show_fav_animes

 def show_fav_animes

    comma_id=cookies[:fav_animes]
    if comma_id == "" || comma_id == nil 
      @fav_animes = AnimeMaster.none
    else
      #放送中アニメ
      @fav_animes = AnimeMaster.where("id IN("+comma_id+") AND state=1").order("day_of_week")
      #放送終了アニメ
      @fav_animes_end = AnimeMaster.where("id IN("+comma_id+") AND state!=1").order("start_at DESC")
    end

 end
  
end
