# _*_ coding: utf-8 _*_ 
class Tasks::UpdateData
  @@logger = MyLogger.get_batch_logger()
  def self.update_chapter
    @@logger.info 'データベースを更新します'
    sql = "update anime_masters,
    (select max(chapter) as max_chapter,anime_master_id from episodes group by anime_master_id)as episode
     set latest_chapter = episode.max_chapter where anime_masters.id = episode.anime_master_id "
    ActiveRecord::Base.connection.execute(sql)
    @@logger.info 'DBを更新しました.'
  end
end
