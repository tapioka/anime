# _*_ coding: utf-8 _*_ 
require 'anemone' 
require 'nokogiri' 
require 'open-uri'
require 'kconv' 
require 'timeout'
require 'uri'
# railsではロードパスのルートがlibになる
require_dependency 'scraper.rb'
require_dependency 'my_logger.rb'
class Tasks::ScrapeBase
  @@logger = MyLogger.get_batch_logger()

  def self.get_doc(url,charset=nil)
    doc = Scraper.get_doc(url,charset)
    return doc
  end




  def self.movie_regix
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end


  def self.get_chapter(movie_text,keyword)
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end

  def self.is_deleted_movie(url)
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end

  def self.get_episode_title(movie)
    # 各話タイトルを取得
    if movie[:title] =~ /「(.*)」/ 
      chapter_title = $1
      @@logger.debug 'タイトル取得　'+chapter_title
      return chapter_title
    end
    return nil
  end
 


  # 最新話を取得
  def self.get_latest_anime(name)
    anime = Episode.find_by_sql([
    'SELECT anime_masters.id , anime_masters.title, MAX(episodes.chapter) as chapter,anime_masters.anitube_word  FROM anime_masters
    INNER JOIN episodes ON anime_masters.id = episodes.anime_master_id 
    INNER JOIN movies ON episodes.id = movies.`episode_id` and name="'+name+'"
    WHERE anime_masters.state=1
    GROUP BY anime_masters.id'
    ])
    return anime
  end


  def self.get_new_episode(anime_id,chapter,movie)
    title = get_episode_title(movie)
    # アニメの⚪︎話がなければ作成
    episode = Episode.find_by(anime_master_id: anime_id, chapter: chapter)
    if episode == nil
      episode = Episode.new(anime_master_id: anime_id, chapter: chapter, title: title)
      @@logger.info chapter.to_s+'話を作成しました'+'\n アニメID: '+anime_id.to_s + '\n タイトル：' + title
    elsif (episode.title == nil || episode.title == "") && title != nil && title != ""
      episode.title=title
      episode.save!
      @@logger.info chapter.to_s+'話をupdateしました'+'\n アニメID: '+anime_id.to_s + '\n タイトル：' + title
      @@logger.debug title
    else
      return nil
    end 
    return episode
  end

  # 取得したエピソードが登録済みか
  def self.is_exist_chapter(anime_id,chapter,name)
    count = Episode.find_by_sql([
    'SELECT episodes.id FROM episodes 
    JOIN movies ON episodes.id = movies.`episode_id` 
    WHERE anime_master_id ='+anime_id.to_s+' AND chapter='+chapter.to_s+' AND name="'+name+'"
    '])
    if count.count <= 0
      @@logger.debug 'false '+'　アニメID:'+anime_id.to_s
      return false
    else
      return true
    end
  end
  

  # 動画リンクをスクレイプ
  def self.scrape_movies(anime)
    if !@keyword
      keyword = anime.title
    else
      keyword = @keyword
    end
    url = URI.escape(@search_url+keyword)
    doc=get_doc(url,@charset)
    if doc == nil
      @@logger.debug '動画が見つかりません'
      return
    end
    movies = doc.xpath(movie_regix)
    get_chapters=[]

    movies.each do |movie|
      chapter = get_chapter(movie,keyword)
      

      if chapter == nil 
        next
      end

      # エピソード取得（タイトルが登録されていないものは登録）
      episode = get_new_episode(anime.id,chapter,movie)

      # 取得したエピソードが登録済みか
      if get_chapters.include?(chapter) 
        next
      end

      # 取得したエピソードが登録済みか(DBから検索)
      if is_exist_chapter(anime.id,chapter,@movie_kind)
        get_chapters.push(chapter)
        next
      end

      # 登録してOKな動画か
      if movie[:href] == nil 
        @@logger.debug "正常なリンクではありません：" + movie
        next
      end
      
      # リンクをフルパスに変換
      movie_link = conv_full_path(url,movie[:href])

      # 動画が削除されてないか確認
      if !is_deleted_movie(movie_link)

        if episode != nil
          episode.save!
        else
          episode = Episode.find_by(anime_master_id: anime.id, chapter:chapter)
        end

        # 動画リンク書き込み
        movie_model=Movie.create!(name: @movie_kind, url: movie_link, episode_id: episode.id)
        @@logger.info '動画を追加しました　' + anime.title + '  ' + chapter.to_s + '話' + @movie_kind
        get_chapters.push(chapter)

      end
    end
  end

  def self.start_scrape
    @@logger.info(self.name.to_s + "を開始します")
    animes = get_latest_anime(@movie_kind)
    animes.each do |anime|
      scrape_movies(anime)
    end
    @@logger.info(self.name.to_s + "を終了します")
  end


  # 相対パスを絶対パスに変換
  def self.conv_full_path(current_url,link)
    return Scraper.conv_full_path(current_url,link) 
  end

end

