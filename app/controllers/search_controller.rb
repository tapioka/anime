class SearchController < ApplicationController
  def index
    @search = AnimeMaster.search do
      fulltext 'BETA' do

        highlight :title
      end
    end
  end
  def show
    word = params[:word]
    page = params[:page]
    if !page
      page = 1
    end
    @search = AnimeMaster.search do
      fulltext word do

        highlight :title
        highlight :discription
      end
      paginate :page => page, :per_page =>10 
    end
  end
end
