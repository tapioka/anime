class RenameInitialColumnToAnimeMasters < ActiveRecord::Migration
  def change
    rename_column :anime_masters, :initial, :initial_id
  end
end
