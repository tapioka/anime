class Admin::TempAnimesController < ApplicationController
  before_action :set_temp_anime, only: [:show, :edit, :update, :destroy]

  # GET /temp_animes
  # GET /temp_animes.json
  def index
    @temp_animes = TempAnime.all
  end

  # GET /temp_animes/1
  # GET /temp_animes/1.json
  def show
  end

  # GET /temp_animes/new
  def new
    @temp_anime = TempAnime.new
  end

  # GET /temp_animes/1/edit
  def edit
  end

  # POST /temp_animes
  # POST /temp_animes.json
  def create
    @temp_anime = TempAnime.new(temp_anime_params)

    respond_to do |format|
      if @temp_anime.save
        format.html { redirect_to admin_temp_anime_url(id:@temp_anime.id), notice: 'Temp anime was successfully created.' }
        format.json { render :show, status: :created, location: @temp_anime }
      else
        format.html { render :new }
        format.json { render json: @temp_anime.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /temp_animes/1
  # PATCH/PUT /temp_animes/1.json
  def update
    respond_to do |format|
      if @temp_anime.update(temp_anime_params)
        format.html { redirect_to @temp_anime, notice: 'Temp anime was successfully updated.' }
        format.json { render :show, status: :ok, location: @temp_anime }
      else
        format.html { render :edit }
        format.json { render json: @temp_anime.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /temp_animes/1
  # DELETE /temp_animes/1.json
  def destroy
    @temp_anime.destroy
    respond_to do |format|
      format.html { redirect_to admin_temp_animes_url, notice: 'Temp anime was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_temp_anime
      @temp_anime = TempAnime.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def temp_anime_params
      params.require(:temp_anime).permit(:title, :start_at, :discription, :initial_id, :state, :day_of_week, :image_path)
    end
end
