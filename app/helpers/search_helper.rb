module SearchHelper
  def get_search_img(img)
    if(img!=nil)

      return img.gsub(/_SL160_/,"_SL80_")
    end
    return "/assets/no_image.png"
  end
end
