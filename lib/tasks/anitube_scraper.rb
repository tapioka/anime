# _*_ coding: utf-8 _*_ 
class Tasks::AnitubeScraper < Tasks::ScrapeBase
  @movie_kind = "Anitube"
  @search_url="http://www.anitube.se/search/?search_id="
  @chaset = 'utf-8'
  

  def self.get_anitube_word(word)
    if word
    word.gsub!(' ','+')
    end
    return word
  end

  def self.movie_regix
    regix = "//div[@id='wrapper']/div[@id='page']/div[@id='mainContent']/div[@class='mainBox']/ul/li[@class='mainList']/div[@class='videoThumb']/a"
    return regix
  end


  def self.get_chapter(movie,keyword)
    if keyword
      word = keyword.gsub!('+',' ')

    else
      word = keyword
    end
    
    if word == nil
      @@logger.debug 'キーワードがないため終了します'
      return
    end

    @@logger.debug word
      if movie[:title] =~ /#{word}|#{word} (\d+)(\s|話|$)/
        @@logger.debug 'エピソード取得' + movie[:title]
        return $1.to_i
      end
      return nil
  end

  def self.is_deleted_movie(url)
    Scraper.is_deleted_anitub(url)
  end

  def self.get_episode_title(movie)
    # タイトルを取得しない
    return nil
  end
  
  def self.start_scrape
    animes = get_latest_anime(@movie_kind)
    animes.each do |anime|
      @keyword = get_anitube_word(anime.anitube_word)
      scrape_movies(anime)
    end
  end


end
