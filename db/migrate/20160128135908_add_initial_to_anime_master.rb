class AddInitialToAnimeMaster < ActiveRecord::Migration
  def change
    add_column :anime_masters, :initial, :string, :limit => 1
    add_column :anime_masters, :state, :integer
  end
end
