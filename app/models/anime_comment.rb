class AnimeComment < ActiveRecord::Base
  belongs_to :anime_master
  validate :add_error
  def add_error
    # commentが空のときにエラーメッセージを追加する
    if comment.blank?
      errors[:base] << "コメントが未入力です"
    end
  end
end
