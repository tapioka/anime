# _*_ coding: utf-8 _*_ 
class Tasks::B9Scraper < Tasks::ScrapeBase
  @movie_kind = "B9"
  @search_url="http://up.b9dm.com/gs.php?keyword="

  def self.movie_regix
    regix = "//div/div/div/div/dl[@class='t_box']/dt/a"
    return regix
  end


  def self.get_chapter(movie,keyword)
      if movie[:title] =~ /(#{keyword}|#{keyword} )(\d+)(\s|話|$)/
        @@logger.debug 'エピソード取得' + movie[:title]
        @@logger.debug $2
        return $2
      end
      return nil
  end

  def self.is_deleted_movie(url)
    return Scraper.is_deleted_b9(url)

    # doc= get_doc(url,"GBK")
    # title = doc.xpath("//title").text
    # if title =~ /削除/
    #   @@logger.debug "削除されました。"
    #   return true;
    # end
    # return false;
  end

  #
  # # 最新話を取得
  # def self.get_latest_anime(name)
  #   anime = Episode.find_by_sql([
  #   'SELECT anime_masters.id , anime_masters.title, MAX(episodes.chapter) as chapter  FROM anime_masters
  #   INNER JOIN episodes ON anime_masters.id = episodes.anime_master_id 
  #   INNER JOIN movies ON episodes.id = movies.`episode_id` and name="'+name+'"
  #   WHERE anime_masters.state=1
  #   GROUP BY anime_masters.id'
  #   ])
  #   return anime
  # end
  #
  #
  # def self.get_episode(anime_id,chapter)
  #   # アニメの⚪︎話がなければ作成
  #   episode = Episode.find_by(anime_master_id: anime_id, chapter: chapter.to_i)
  #   if(episode == nil)
  #     episode = Episode.create!(anime_master_id: anime_id, chapter: chapter.to_i)
  #     @@logger.debug chapter.to_s+'話を作成しました'+' アニメID: '+anime_id.to_s
  #   end 
  #   return episode
  # end
  #
  # # 取得したエピソードが登録済みか
  # def self.is_exist_chapter(anime_id,chapter,name)
  #   count = Episode.find_by_sql([
  #   'SELECT episodes.id FROM episodes 
  #   JOIN movies ON episodes.id = movies.`episode_id` 
  #   WHERE anime_master_id ='+anime_id.to_s+' AND chapter='+chapter.to_s+' AND name="'+name+'"
  #   '])
  #   if count.count <= 0
  #     @@logger.debug 'false '+'　アニメID:'+anime_id.to_s
  #     return false
  #   else
  #     return true
  #   end
  # end
  #
  #
  # def self.get_chapter_title(movie,episode)
  #   # 各話タイトルを取得
  #   if movie.text =~ /「(.*)」/ && episode.title == nil
  #     chapter_title = $1
  #     episode.title = chapter_title
  #     episode.save!
  #     @@logger.debug 'タイトル取得　'+movie.text
  #   end
  # end
  #
  #
  #
  # # 動画リンクをスクレイプ
  # def self.scrape_movies(anime)
  #   keyword = anime.title
  #   url = URI.escape(@search_url+keyword)
  #   doc=get_doc(url)
  #   movies = doc.xpath(movie_regix)
  #   get_chapters=[]
  #
  #   movies.each do |movie|
  #     chapter = get_chapter(movie.text,keyword)
  #     if chapter != nil && !is_exist_chapter(anime.id,chapter,@movie_kind) && movie[:href] != nil && !get_chapters.include?(chapter) && !is_deleted_movie(movie[:href])
  #
  #       # アニメの⚪︎話がなければ作成
  #       episode = get_episode(anime.id,chapter)
  #
  #       # 各話タイトルを取得
  #       get_chapter_title(movie,episode)
  #
  #       # 動画リンク書き込み
  #       movie_model=Movie.create!(name: @movie_kind, url: movie[:href], episode_id: episode.id)
  #       @@logger.debug '動画を追加しました　' + anime.title + '  ' + chapter.to_s + '話'
  #       get_chapters.push(chapter)
  #
  #     end
  #   end
  # end
  #
  # def self.start_scrape
  #   animes = get_latest_anime(@movie_kind)
  #   animes.each do |anime|
  #
  #     scrape_movies(anime)
  #     # movies = doc.xpath("//div/div/div/div/dl[@class='t_box']/dt/a")
  #     # movies.each do |movie|
  #     #   if movie.text =~ /#{keyword}.*(\d+)(\s|話|$)/
  #     #     chapter = $1.to_i
  #     #     if !is_exist_chapter(anime.id,chapter,@movie_kind) 
  #     #       && movie[:href] != nil 
  #     #       && !get_chapters.include?(chapter) 
  #     #       && !is_deleted_B9(movie[:href])
  #     #       
  #     #       # アニメの⚪︎話がなければ作成
  #     #       episode = get_episode(anime.id,chapter)
  #     #       
  #     #       # 各話タイトルを取得
  #     #       get_chapter_title(movie,episode)
  #     #       
  #     #       # 動画リンク書き込み
  #     #       movie_model=Movie.create!(name: @movie_kind, url: movie[:href], episode_id: episode.id)
  #     #       get_chapters.push(chapter)
  #     #     
  #     #     end
  #     #   end
  #     # end
  #   end
  # end
end
