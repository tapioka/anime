module AnimeHelper
  def get_big_img(img)
    if(img!=nil)
      return img.gsub(/_SL160_/,"_SL600_")
    end
      return image_path("no_image.png");
  end
  def get_list_img(img)
    if(img!=nil)
      return img
    end
    return "/assets/no_image.png"
  end
  def get_wday(day_of_week)
    case day_of_week
      when 0 then
        return "日曜"
      when 1 then
        return "月曜"
      when 2 then
        return "火曜"
      when 3 then
        return "水曜"
      when 4 then
        return "木曜"
      when 5 then
        return "金曜"
      when 6 then
        return "土曜"
      else
        return ""
    end
  end
end
