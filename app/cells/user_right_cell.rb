class UserRightCell < Cell::ViewModel
  include AnimeHelper

  def show
    temp_cookie= request.headers[:Cookie]
    if(/cView=(.+?);/ =~ temp_cookie )
      ids =  URI.unescape($1)
      if ids.start_with?(";")
        continue_views = AnimeMaster.none
      else
        continue_views = AnimeMaster.where("id IN("+ids+")").order("day_of_week")
      end
    else
      continue_views = AnimeMaster.none
    end
    render locals: { continue_views: continue_views }
  end

end
