require 'test_helper'

class TempAnimesControllerTest < ActionController::TestCase
  setup do
    @temp_anime = temp_animes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:temp_animes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create temp_anime" do
    assert_difference('TempAnime.count') do
      post :create, temp_anime: { day_of_week: @temp_anime.day_of_week, discription: @temp_anime.discription, image_path: @temp_anime.image_path, initial_id: @temp_anime.initial_id, start_at: @temp_anime.start_at, state: @temp_anime.state, title: @temp_anime.title }
    end

    assert_redirected_to temp_anime_path(assigns(:temp_anime))
  end

  test "should show temp_anime" do
    get :show, id: @temp_anime
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @temp_anime
    assert_response :success
  end

  test "should update temp_anime" do
    patch :update, id: @temp_anime, temp_anime: { day_of_week: @temp_anime.day_of_week, discription: @temp_anime.discription, image_path: @temp_anime.image_path, initial_id: @temp_anime.initial_id, start_at: @temp_anime.start_at, state: @temp_anime.state, title: @temp_anime.title }
    assert_redirected_to temp_anime_path(assigns(:temp_anime))
  end

  test "should destroy temp_anime" do
    assert_difference('TempAnime.count', -1) do
      delete :destroy, id: @temp_anime
    end

    assert_redirected_to temp_animes_path
  end
end
