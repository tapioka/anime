json.array!(@anime_masters) do |anime_master|
  json.extract! anime_master, :id, :title, :start_at, :discription
  json.url anime_master_url(anime_master, format: :json)
end
