# _*_ coding: utf-8 _*_ 
require 'anemone' 
require 'nokogiri' 
require 'open-uri'
require 'kconv' 
require 'timeout'
require 'uri'
class Scraper

  def self.get_doc(url,charset=nil)
      sleep(1)

      userAgent = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)'
      options = {
      "Accept-Language" => "ja,en;q=0.5",
      'User-Agent' =>userAgent,
      }
      timeout = 5
      doc = nil
    begin
      Timeout::timeout(timeout) do
        html = open(url, options) do |f|
          if charset == nil
            charset = f.charset # 文字種別を取得
            if charset == "iso-8859-1"
              charset="utf-8"
            end
          end
          f.read # htmlを読み込んで変数htmlに渡す
        end
      doc = Nokogiri::HTML.parse(html,nil,charset) 
      end
    rescue TimeoutError => e
      p 'タイムアウト'+e.to_s
      return nil
    rescue => exception
      case exception
        when OpenURI::HTTPError # 404エラ
          puts "404エラー"+url
        else
          puts "その他エラー"
          puts exception.to_s
      end
      return nil
    end
    return doc
  end


  # 相対パスを絶対パスに変換
  def self.conv_full_path(current_url,link)
    
    if link =~ /^http/
      link = URI.escape(link)
      return link
    else
      link = URI.join(current_url,link)
    end
    return link
  end


  def self.is_deleted_movie(url,movie_name)
    case movie_name
    when 'B9' 
      return is_deleted_b9(url)
    when 'ひまわり'
      return is_deleted_himawari(url) 
    when 'AniTube'
      return is_deleted_anitube(url)
    else 
      return true
    end
  end

  def self.is_deleted_b9(url)
    doc= get_doc(url,"GBK")
    if doc == nil
      return true
    end
    title = doc.xpath("//title").text
    if title =~ /削除/
      p "削除されました。"
      return true;
    end
    return false;
  end

  def self.is_deleted_himawari(url)
    p "動画が生きてるか確認します"
    doc= get_doc(url,"utf-8")
    if doc == nil 
      return true
    end
    temp_text = doc.xpath("//div[@id='content']/div[@id='topmovie_left_box']/div[@id='topmovie']")
    if temp_text

      text = temp_text.xpath("//div[@id='link_disablemessag_own']").text

      if text =~ /削除/
        p "削除されました。"
        return true;
      end

      text = temp_text.xpath("//div[@id='link_disablemessage_rights']").text
      if text =~ /削除/
        p "削除されました。"
        return true;
      end

    else

        p "削除されました。"
        return true;

    end

    p "動画は生きています"
    return false;
  end

  def self.is_deleted_anitube(url)
    p "動画が生きてるか確認します"
    p url
    doc= get_doc(url,"utf-8")
    if doc == nil 
      return true
    end
    text = doc.xpath("//div/div[@id='header']/div").text

    if text =~ /Video inexistente, pode ter sido apagado ou marcado como inapropriado!/
      p "削除されました。"
      return true;
    end
    
    p "動画は生きています"
    return false;
  end

end

