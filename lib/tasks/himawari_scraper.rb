# _*_ coding: utf-8 _*_ 
class Tasks::HimawariScraper < Tasks::ScrapeBase
  @movie_kind = "ひまわり"
  @search_url="http://himado.in/?keyword="

  def self.movie_regix
    regix = "//div[@id='content']/div[@id='leftbox']/table/tr/td/table/tr/td/div/a"
    return regix
  end


  def self.get_chapter(movie,keyword)
      if movie[:title] =~ /#{keyword}.*(\d+)(\s|話|$)/
        @@logger.debug 'エピソード取得' + movie[:title]
        return $1.to_i
      end
      return nil
  end

  def self.is_deleted_movie(url)
    return Scraper.is_deleted_himawari(url)
  end

end
