class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.string :title
      t.integer :chapter
      t.integer :anime_id

      t.timestamps null: false
    end
  end
end
