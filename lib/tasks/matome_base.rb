# _*_ coding: utf-8 _*_ 
require 'anemone' 
require 'nokogiri' 
require 'open-uri'
require 'kconv' 
require 'timeout'
require 'uri'
# railsではロードパスのルートがlibになる
require_dependency 'scraper.rb'
require_dependency 'my_logger.rb'
class  Tasks::MatomeBase
  @base_url = 'http://tvanimedouga.blog93.fc2.com/'
  @animes = nil
  @@logger = MyLogger.get_batch_logger()

  def self.get_doc(url,charset=nil)
    doc = Scraper.get_doc(url,charset)
    return doc
  end

  # 相対パスを絶対パスに変換
  def self.conv_full_path(current_url,link)
    return Scraper.conv_full_path(current_url,link) 
  end

  # 最新話を取得
  def self.get_latest_anime()
    day = Time.now
    wday = day.wday
    bwday = wday == 0 ? 6 : wday.to_i-1
    anime =  AnimeMaster.where(state:1).where("day_of_week = "+wday.to_s + " OR day_of_week = "+ bwday.to_s )
    return anime
  end


  # # 放送中のアニメをスクレイプ
  # def self.scrape_latest_animes
  #   animes = get_latest_anime()
  #   animes.each do |anime|
  #     scrape_animes(anime)
  #   end
  # end

  # アニメをスクレイプ
  def self.scrape_animes()
    @@logger.info(self.name.to_s + "を開始します")
    doc = get_doc(@base_url)
    if doc == nil
      @@logger.debug 'ページを取得できませんでした'
      return
    end

    # 目的のアニメのリンクを取得
    anime_links = get_anime_links(doc)
    
    if anime_links == nil
      @@logger.debug 'アニメリンクが見つかりません'
      return
    end
    
    anime_links.each do |anime_link|

      # スクレイプ対象のアニメか確認
      anime = can_scrape_anime(anime_link)
      if anime == nil
        next
      end

      # 各話ページの内容を取得
      next_url = conv_full_path(@base_url,anime_link[:href])
      chapter_doc = get_doc(next_url)
      if chapter_doc ==nil
        @@logger.debug '各話ページが取得できませんでした'
        next
      end

      # 各話ページをスクレイプ
      scrape_chapter(anime,chapter_doc,next_url)

    end
    @@logger.info(self.name.to_s + "を終了します")
  end


  # 目的のアニメのリンクを取得
  def self.get_anime_links(doc)
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end

  # スクレイプ対象のアニメか確認
  def self.can_scrape_anime(anime_link,anime)
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end




  # 各話ページをスクレイプ
  def self.scrape_chapter(anime,chapter_doc,current_url)
    # 動画のAタグを取得
    movie_list_links = get_movie_list_links(chapter_doc)
    if movie_list_links ==nil
      @@logger.debug '動画ページへのリンクが見つかりませんでした'
      return
    end
    movie_list_links.each do |movie_list_lnk|
      #スクレイプ対象の話か確認
      episode = get_episode(movie_list_lnk,anime)
      if episode == nil
        next
      end
      # 動画ページの内容を取得
      next_url = conv_full_path(current_url,movie_list_lnk[:href])
      movie_doc = get_doc(next_url)
      if movie_doc ==nil
        @@logger.debug '動画ページが取得できませんでした'
        next
      end

      # 動画ページをスクレイプ
      scrape_movie(episode,movie_doc)

    end

  end

# 動画のAタグを取得
  def self.get_movie_list_links(doc)
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end

  # スクレイプ対象の話か確認(OKならEpisodeモデルを返す)
  def self.get_episode(movie_list_link,anime)
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end

  # 各話タイトルを取得
  def self.get_episode_title(movie)
    if movie[:title] =~ /「(.*)」/ 
      chapter_title = $1
      @@logger.debug 'タイトル取得　'+chapter_title
      return chapter_title
    end
    return nil
  end


  def self.scrape_movie(episode,movie_doc)
    # リンクを取得
    movie_links = get_movie_links(movie_doc)
    if movie_links == nil
      return
    end
    movie_links.each do |movie_link|
      # スクレイプ対象のリンクだったらmovieモデルを返す
      movie_model =  get_movie_model(movie_link,episode)
      if movie_model == nil
        next
      end
      movie_model.save!
      @@logger.info '動画を登録しました' + '\n アニメID:' + episode.anime_master_id.to_s + '\n エピソード：' + episode.chapter.to_s 
    end

    
  end

  # リンクを取得
  def self.get_movie_links(movie_doc)
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end

  # スクレイプ対象の動画か確認
  def self.get_movie_model(movie_link,episode)
    raise NotImplementedError.new("#{self.class.name}.#{current_method_name} is an abstract method.")
  end


  # 記録していい動画かを確認
  def self.is_scrape_movie(movie_name,anime_id,chapter)
    anime = Episode.find_by_sql([
      '
     SELECT episodes.id  FROM episodes
     INNER JOIN movies ON movies.episode_id = episodes.id
     AND movies.name = "'+movie_name+'"                                          
     WHERE episodes.anime_master_id = '+anime_id.to_s+' AND episodes.chapter = '+chapter.to_s
    ])
     if anime.count > 0
       return false
     end
    return true
  end

end

