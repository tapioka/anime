class EpisodeController < ApplicationController
  def show
    @episode = Episode.find(params[:id])
    @new_comment = EpisodeComment.new
    @new_comment.episode_id = params[:id]
    @episode_comments = EpisodeComment.where(episode_id: params[:id]).order("updated_at desc")
  end
end
