# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

require File.expand_path(File.dirname(__FILE__) + "/environment.rb")
# 出力先のログファイルの指定
set :output, { error: "log/cron_error.log" }
# # ジョブの実行環境の指定
set :environment, :production

# 毎日 am3:30のスケジューリング
every 1.day, at: '3:30 am' do
  # p 'B9スクレイプ'
  runner "Tasks::B9Scraper.start_scrape"
end

every 1.day, at: '3:40 am' do
  # p 'HIMAWARIスクレイプ'
  runner "Tasks::HimawariScraper.start_scrape"
end

every 1.day, at: '3:50 am' do
  # p 'FC2スクレイプ'
  runner "Tasks::Fc2Scraper.scrape_animes"
end

every 1.day, at: '4:30 am' do
  runner "Tasks::UpdateData.update_chapter"
end
# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
