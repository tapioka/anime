class UserLeftCell < Cell::ViewModel
    include AnimeHelper
  def show
    this_week_animes=AnimeMaster.where(state:1).order("day_of_week")
    day_of_week = ["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"]
  end_animes = AnimeMaster.where(state:2).order("title")
  render locals: { this_week_animes: this_week_animes, day_of_week: day_of_week, end_animes: end_animes}
  end

end
