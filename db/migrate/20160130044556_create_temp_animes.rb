class CreateTempAnimes < ActiveRecord::Migration
  def change
    create_table :temp_animes do |t|
      t.string :title
      t.date :start_at
      t.text :discription
      t.integer :initial_id
      t.integer :state
      t.integer :day_of_week
      t.text :image_path

      t.timestamps null: false
    end
  end
end
