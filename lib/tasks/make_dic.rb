# _*_ coding: utf-8 _*_ 
require 'csv'
class Tasks::MakeDic
  def self.output(title, type)
    title_length = title.length
    return nil unless title_length > 3

    score = [-36000.0, -400 * (title_length ** 1.5)].max.to_i
    [title, nil, nil, score, '名詞', '一般', '*', '*', '*', '*', title, '*', '*', type]
  end
  def self.exec
    require 'csv'


    CSV.open("user.csv", 'w') do |csv|
      # niconico
      Dir::foreach('./niconico') do |f|
        next unless f =~ /^head[0-9]{4}\.csv$/
        open("./niconico/#{f}").each do |line|
          title = line.split(',')[1].gsub('"','')
          out = output(title, 'niconico')
          csv << out unless out.nil?
        end
      end

      # hatena
      open('keywordlist_furigana.csv').each do |line|
        title = line.split("\t")[1].strip

        out = output(title, 'hatena')
        csv << out unless out.nil?
      end

      # Wikipedia
      open('jawiki-latest-all-titles-in-ns0').each do |line|
        title = line.strip

        next if title =~ %r(^[+-.$()?*/&%!"'_,]+)
        next if title =~ /^[-.0-9]+$/
        next if title =~ /曖昧さ回避/
        next if title =~ /_\(/
        next if title =~ /^PJ:/
        next if title =~ /の登場人物/
        next if title =~ /一覧/

        title.gsub!('_', ' ')

        out = output(title, 'wikipedia')
        csv << out unless out.nil?
      end
    end
  end
end
