class CreateInitials < ActiveRecord::Migration
  def change
    create_table :initials do |t|
      t.string :name
      t.integer :initial_group_id

      t.timestamps null: false
    end
  end
end
