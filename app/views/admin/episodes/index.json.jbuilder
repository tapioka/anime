json.array!(@episodes) do |episode|
  json.extract! episode, :id, :title, :chapter, :anime_id
  json.url episode_url(episode, format: :json)
end
