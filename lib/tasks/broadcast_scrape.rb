# _*_ coding: utf-8 _*_ 
require 'anemone' 
require 'nokogiri' 
require 'open-uri'
require 'kconv' 
require 'timeout'
class Tasks::BroadcastScrape
  def self.getDoc(url)
    charset = nil
      sleep(1)
      userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36'
    begin
      pxy = "http://157.7.242.101:8080"
      usr = ""
      pss = ""
        html = open(url, 'User-Agent' =>userAgent,
                     :proxy_http_basic_authentication => [pxy,usr,pss] ) do |f|
          charset = f.charset # 文字種別を取得
          f.read # htmlを読み込んで変数htmlに渡す
        end
      doc = Nokogiri::HTML.parse(html,nil,charset) 
    rescue => exception
      case exception
        when OpenURI::HTTPError # 404エラ
          puts "404エラー"+url
        else
          puts "その他エラー"
          puts exception.to_s
      end
      return nil
    end
    return doc
  end
  def self.execute
    titles =""

    anime_list_url="http://probu.jp"
    base_url = "http://r.mag-u.jp/i-anime/season/"
    (1981..2016).reverse_each do |i|
      doc=getDoc(base_url+i.to_s)
      puts doc.xpath("//title")
      temp_data = doc.xpath("//div[@style='font-size:100%; margin-top:10px; padding-bottom:10px; border-bottom:1px solid #eee;']")
      temp_data.each do |data|
        title= data.css("font").inner_text
        /(\d+)\/(\d+)\/(\d+)/=~data.text
        start_at=""
        day_of_week=""
        begin
          start_at = Date.new($1.to_i,$2.to_i,$3.to_i)
          day_of_week = start_at.wday
        rescue => e
          p e.to_s
          next
        end
        temp_anime=TempAnime.new()
        temp_anime.title=title
        temp_anime.start_at=start_at
        temp_anime.day_of_week=day_of_week
        temp_anime.save
      end

    end
  end
end

