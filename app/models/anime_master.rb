class AnimeMaster < ActiveRecord::Base
  has_many:episodes,:dependent => :destroy

  searchable do
    text :title, :stored => true

    text :discription, :stored => true
  end
end
