Rails.application.routes.draw do
  namespace :admin do
    resources :temp_animes
    resources :initials
    resources :movies
    resources :episodes
    resources :anime_masters
  end
  resources :episode_comments, :only => [:create,:show] 
  resources :anime_comments, :only => [:create,:show]
  resources :anime, :only => [:index, :show]
  resources :episode, :only => [:show]
  post 'fav_animes/add', to: 'fav_animes#add' 
  get 'search', to: 'search#show'
  root to: 'anime#index' 
end
