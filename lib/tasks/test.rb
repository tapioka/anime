# _*_ coding: utf-8 _*_ 
class Tasks::Test
  require 'mecab'
  require 'matrix'
  require 'uri'

  def self.test(text)

    if text =~ /【(?!.*検索)(.*)】/
      p $1
    end
  end
  def self.arasuji_fix

    animes = AnimeMaster.all
    animes.each do |anime|
      if anime.discription =~ /あらすじ(.*)$/
        if $1 != nil
        anime.discription = $1
        anime.save!
        p $1
        end
      elsif anime.discription =~ /ストーリー(.*)$/
        if $1 != nil
          anime.discription = $1
          anime.save!
          p $1
        end
      end
    end
  end
  def self.title_fix
    episodes = Episode.all
    episodes.each do |episode|
      if episode.title =~ /「(.*)」/
        episode.title = $1
        p $1
        episode.save!
        next
      end
      if episode.title =~ /^\d+話$/
        episode.title = nil
        p episode.title
        episode.save!
        next
      end
    end
  end
  
  # コサイン類似度を計算
  # @param [String] str1 文字列
  # @param [String] str2 文字列
  # @return [Float] スコア
  def self.calc_score(str1,str2)
    vector = []
    vector1 = []
    vector2 = []
    frag_vector1 = []
    frag_vector2 = []

    node1 = MeCab::Tagger.new.parseToNode(str1)
    node2 = MeCab::Tagger.new.parseToNode(str2)

    while node1
      vector1.push(node1.surface)
      p vector1
      node1 = node1.next
    end

    while node2
      vector2.push(node2.surface)
      p vector2
      node2 = node2.next
    end

    vector += vector1
    vector += vector2

    #重複と空文字列を削除
    vector.uniq!.delete("")
    vector1.delete("")
    vector.delete("")
    vector2.delete("")

    vector.each do |word|
      if vector1.include?(word) then
        frag_vector1.push(1)
      else
        frag_vector1.push(0)
      end

      if vector2.include?(word) then
        frag_vector2.push(1)
      else
        frag_vector2.push(0)
      end
    end

    vector1_final = Vector.elements(frag_vector1, copy = true)
    vector2_final = Vector.elements(frag_vector2, copy = true)

    return vector2_final.inner_product(vector1_final)/(vector1_final.norm() * vector2_final.norm())

  end

  def self.exec
    p calc_score("AMNESIA -アムネシア-","アムネシア")

  end
end
