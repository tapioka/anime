/*!
 * jQuery Cookie Plugin
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function($) {
    $.cookie = function(key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) { return s; } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
})(jQuery);
// $(document).ready(ready);
// $(document).on('page:load', ready);
// function ready(){
//
// 	var fav_animes = $.cookie('fav_animes');
// show(fav_animes);
// }
function continue_view(id){

	// JSONを連想配列に復元
	var fav_animes = $.cookie('fav_animes');
	if(fav_animes == null || fav_animes == ""){
		fav_animes = id;
		$.cookie( "fav_animes" , fav_animes, {path:'/'} ); 
show(fav_animes);
		return;
	}
	var arrCV = fav_animes.split(",")
	// if(arrCV.length > 5){
	// 	alert("ログインしてください");
	// }
	// else{
		if(arrCV.indexOf(id.toString())==-1){
			fav_animes += "," + id
		}
	// }

	$.cookie( "fav_animes" , fav_animes ,{path:'/'}); 
show(fav_animes);
}
function del_cview(id){
	temp_fav_animes = $.cookie( "fav_animes"); 
	var arrCV = temp_fav_animes.split(",");
	var fav_animes="";
	arrCV.splice(arrCV.indexOf(id.toString()),1);
	arrCV.forEach(function(val){
		fav_animes+=","+val;
	});
	fav_animes=fav_animes.substr(1);
	$.cookie( "fav_animes" , fav_animes ,{path:'/'}); 
show(fav_animes);
		// location.reload();

}
function show(id){
	if(id==""){
	id=0;
	}
	$.ajax({
		// url: '/fav_animes/add/'+id,
		url: '/fav_animes/add',
		// method: 'GET',
		method: 'POST',
		data: { id: id },
		success: function (msg) {
			$('#continue_views').html(msg);
		},
	});
}
function reload_c_view(){
    var
        History = window.History, // Note: We are using a capital H instead of a lower h
        State = History.getState(),
        $log = $('#log');

        $.ajax({
            url: State.url,
            success: function (msg) {
                $('#continue_views').html($(msg).find('#continue_views').html());
            }
        });

}
