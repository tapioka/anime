class MyLogger
  def self.get_batch_logger()
    logger = ActiveSupport::Logger.new("log/batch.log", "daily")
    console = ActiveSupport::Logger.new(STDOUT)

    logger.extend ActiveSupport::Logger.broadcast(console)
    logger.formatter = ::Logger::Formatter.new
    return logger
  end
    
end
