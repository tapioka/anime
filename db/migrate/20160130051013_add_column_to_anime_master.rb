class AddColumnToAnimeMaster < ActiveRecord::Migration
  def change
    add_column :anime_masters, :day_of_week, :integer
    add_column :anime_masters, :image_path, :text
  end
end
