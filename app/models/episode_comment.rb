class EpisodeComment < ActiveRecord::Base
  belongs_to :episode
  validate :add_error
  def add_error
    # commentが空のときにエラーメッセージを追加する
    if comment.blank?
      errors[:base] << "コメントが未入力です"
    end
  end
end
